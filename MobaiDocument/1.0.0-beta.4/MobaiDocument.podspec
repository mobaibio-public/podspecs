Pod::Spec.new do |s|
    s.name             = 'MobaiDocument'
    s.version          = '1.0.0-beta.4'
    s.summary          = 'MobaiDocument SDK'
  
    s.description      = <<-DESC
      Mobai iOS SDK provides functionality for face capture using the built in front facing camera, and API client for 
      interaction with the Mobai API
                         DESC
  
    s.homepage         = 'https://www.mobai.dev'
    s.license          = { :type => 'commercial', :file => 'LICENSE' }
    s.author           = { 'Mobai' => 'support@mobai.bio' }
    s.source           = { :http => 'https://downloads.mobai.dev/releases/ios/1.0.0-beta.4/MobaiDocument.xcframework.zip' }
    
    s.platform = :ios
    s.swift_version = '5.0'
    s.module_name = 'MobaiDocument'
    s.ios.deployment_target = '13.4'
    s.ios.vendored_frameworks = 'MobaiDocument.xcframework'
  
    s.pod_target_xcconfig = { 
      'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES',
    }
  
    s.dependency 'dot-document', "~> 6.2.0"
    s.dependency 'dot-nfc', "~> 6.2.0"
  end
  