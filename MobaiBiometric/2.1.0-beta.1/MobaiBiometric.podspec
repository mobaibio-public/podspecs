Pod::Spec.new do |s|
    s.name             = 'MobaiBiometric'
    s.version          = '2.1.0-beta.1'
    s.summary          = 'MobaiBiometric SDK'
  
    s.description      = <<-DESC
      Mobai iOS SDK provides functionality for face capture using the built in front facing camera, and API client for 
      interaction with the Mobai API
                         DESC
  
    s.homepage         = 'https://www.mobai.dev'
    s.license          = { :type => 'commercial', :file => 'LICENSE' }
    s.author           = { 'Mobai' => 'support@mobai.bio' }
    s.source           = { :http => 'https://downloads.mobai.dev/releases/ios/2.1.0-beta.1/MobaiBiometric.xcframework.zip' }
    
    s.platform = :ios
    s.swift_version = '5.0'
    s.module_name = 'MobaiBiometric'
    s.ios.deployment_target = '13.4'
    s.ios.vendored_frameworks = 'MobaiBiometric.xcframework'
  
    s.pod_target_xcconfig = { 
      'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES',
    }
  
    s.dependency 'SwiftProtobuf', '~> 1.0'
  end
  