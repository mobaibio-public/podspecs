Pod::Spec.new do |s|
    s.name             = 'MobaiCore'
    s.version          = '1.0.0'
    s.summary          = 'MobaiCore SDK'
  
    s.description      = <<-DESC
      Mobai iOS SDK provides functionality for face capture using the built in front facing camera, and API client for 
      interaction with the Mobai API
                         DESC
  
    s.homepage         = 'https://www.mobai.dev'
    s.license          = { :type => 'commercial', :file => 'LICENSE' }
    s.author           = { 'Mobai' => 'support@mobai.bio' }
    s.source           = { :http => 'https://downloads.mobai.dev/releases/ios/swift/1.0.0/MobaiCore.xcframework.zip' }
    
    s.platform = :ios
    s.swift_version = '5.0'
    s.ios.deployment_target = '13.0'
    s.ios.vendored_frameworks = 'MobaiCore.xcframework'

    s.pod_target_xcconfig = { 
      'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64',
      'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES'
    }

    s.user_target_xcconfig = { 
      'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' 
    }

    s.dependency 'SwiftProtobuf', '~> 1.0'
  end


  