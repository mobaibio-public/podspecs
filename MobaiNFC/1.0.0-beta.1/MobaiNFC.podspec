Pod::Spec.new do |s|
    s.name             = 'MobaiNFC'
    s.version          = '1.0.0-beta.1'
    s.summary          = 'Mobai NFC reader'
  
    s.description      = <<-DESC
      Provides functionality to read 
                         DESC
  
    s.homepage         = 'https://www.mobai.dev'
    s.license          = { :type => 'commercial', :file => 'LICENSE' }
    s.author           = { 'Mobai' => 'support@mobai.bio' }
    s.source           = { :http => 'https://downloads.mobai.dev/releases/ios/1.0.0-beta.1/MobaiNFC.xcframework.zip' }
    
    s.platform = :ios
    s.swift_version = '5.0'
    s.module_name = 'MobaiNFC'
    s.ios.deployment_target = '13.4'
    s.ios.vendored_frameworks = 'MobaiNFC.xcframework'
  
    s.pod_target_xcconfig = { 
      'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES',
    }
  
    s.dependency 'dot-nfc', "= 8.3.0"
  end
  