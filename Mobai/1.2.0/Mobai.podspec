Pod::Spec.new do |s|
    s.name             = 'Mobai'
    s.version          = '1.2.0'
    s.summary          = 'Mobai SDK'
  
    s.description      = <<-DESC
      Mobai iOS SDK provides functionality for face capture using the built in front facing camera, and API client for 
      interaction with the Mobai API
                         DESC
  
    s.homepage         = 'https://www.mobai.dev'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'Mobai' => 'support@mobai.bio' }
    s.source           = { :http => 'https://downloads.mobai.dev/releases/ios/swift/1.2.0/Mobai.xcframework.zip' }
    
    s.ios.deployment_target = '13.0'
    s.swift_version = '5.0'
    s.vendored_frameworks = 'Mobai.xcframework'

    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  end


  